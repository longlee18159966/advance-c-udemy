#include <iostream>
#include <complex>
#include <complex.h>


using namespace std;


int main (void)
{
    #ifdef __STDC_NO_COMPLEX__
    cout << "Not support complex number" << endl;
    #else 
    cout << "Support complex number" << endl;

    double _Complex ix = 1 + 2*I;
    double _Complex yx = 3 + 4*I;
    double _Complex s  = ix + yx;
    double _Complex d  = ix - yx;
    double _Complex v  = ix * yx;
    double _Complex n  = ix / yx;

    /* Luu y su dung format %f%+fi de khong bi mat dau cua phan ao */
    printf("s = %.2f%+.2fi\n", creal(s), cimag(s));
    printf("d = %.2f%+.2fi\n", creal(d), cimag(d));
    printf("v = %.2f%+.2fi\n", creal(v), cimag(v));
    printf("n = %.2f%+.2fi\n", creal(n), cimag(n));
    #endif


    return 0;
}