/* 
 * Write a program that uses a flexible array member inside a structure
 * Create a structure named "MyArray" that has a length member and a fexible array member named array
 * Read in from the user the size of the array at runtime
 * Allocate memory for the structure based on this size read in from the user
 * Set the length member and fill the array with some dummy data
 * Print the array elements
 */

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <iostream>

using namespace std;


typedef struct 
{
    int size;
    int array[];
} MyArray;

int main (void)
{
    MyArray *UserArray;
    int b;
    cout << "Enter the size of array" << endl;
    cin >> b;
    UserArray = (MyArray *) malloc(sizeof(MyArray) + b * sizeof(int));
    UserArray->size = b;
    int i;
    for (int i = 0; i < UserArray->size; ++i)
    {
        UserArray->array[i] = i + 1;
    }
    cout << "Array elements: ";
    for (int i = 0; i < UserArray->size; ++i)
    {
        cout << UserArray->array[i] << '\t';
    }

    return 0;
}
