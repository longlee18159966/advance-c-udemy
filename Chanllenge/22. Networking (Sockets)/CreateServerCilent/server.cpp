/*
 * step to using sokcets to communicate
 *
 * 1. create a new socket for network communication
 * 
 * 2. attach a local address to a socket (bind)
 * 
 * 3. announce willingness to accept connections (listen)
 * 
 * 4. block caller until a connection request arrives (accept)
 * 
 * 5. send some data over connection (send)
 * 
 * 6. receive some data over connection (receive)
 *  
 * 7. release the connection (close)
 */

#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

short SocketCreate (void)
{
    short hSocket;
    cout << "Create the socket" << endl;
    hSocket = socket(AF_INET, SOCK_STREAM, 0);

    return hSocket;
}

int bindCreatedSocket (int hSocket)
{
    int iRetval = -1;
    int cilentPort = 12345;
    struct sockaddr_in remote = {0};

    /* internet address family */
    remote.sin_family = AF_INET;

    /* Any incoming interface */
    remote.sin_addr.s_addr = htonl(INADDR_ANY);
    remote.sin_port        = htons(cilentPort);

    iRetval = bind(hSocket, (struct sockaddr *)&remote, sizeof(remote));

    return iRetval;
}

int main (void)
{
    int socket_desc = 0, sock = 0, cilentLen = 0;
    struct sockaddr_in cilent;
    char cilent_message[200] = {0};
    char message[100] = {0};
    const char *pMessage = "Hello from these other side";
    
    /* create socket */
    socket_desc = SocketCreate();

    if (socket_desc == -1)
    {
        cout << "Could not create socket" << endl;
        return 1;
    }
    cout << "Socket created" << endl;
    /* bind */
    if (bindCreatedSocket(socket_desc) < 0)
    {
        /* pirnt error message */
        cout << "Bind failed" << endl;
        return 1;
    }
    cout << "Bind done!" << endl;

    /* listen */
    listen(socket_desc, 3);

    /* accept incoming connection */

    for (;;)
    {
        cout << "Waiting for incoming connections..." << endl;
        cilentLen = sizeof(struct sockaddr_in);

        /* accept connection from an incoming cilent */
        sock = accept(socket_desc, (struct sockaddr *)&cilent, (socklen_t *)&cilentLen);
        if (sock < 0)
        {
            cout << "accept falied!" << endl;
            return 1;
        }
        cout << "Connection accepted" << endl;
        memset(cilent_message, '\0', sizeof(cilent_message));
        memset(message, '\0', sizeof(message));

        /* receive a reply from the cilent */
        if (recv(sock, cilent_message, 200, 0) < 0)
        {
            cout << "receive failed" << endl;
            break;
        }

        cout << "Cilent reply: " << cilent_message << endl;

        if (strcmp(pMessage, cilent_message) == 0)
        {
            strcpy(message, "Hi there!");
        }
        else
        {
            strcpy(message, "Invalid message");
        }

        /* send some data */
        if (send(sock, message, strlen(message), 0) < 0)
        {
            cout << "Send failed" << endl;
            return 1;
        }
        close(sock);
        sleep(1);
    }

    return 0;
}
