

#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

short SocketCreate (void)
{
    short hSocket;
    cout << "Create the socket" << endl;
    hSocket = socket(AF_INET, SOCK_STREAM, 0);

    return hSocket;
}


/* try to connect with server */
int socketConnect (int hSocket)
{
    int iRetval = -1;
    int ServerPort = 12345;

    struct sockaddr_in remote = {0};
    remote.sin_addr.s_addr = inet_addr("127.0.0.1");
    remote.sin_family = AF_INET;
    remote.sin_port = htons(ServerPort);

    iRetval = connect(hSocket, (struct sockaddr *)&remote, sizeof(remote));

    return iRetval;
}


int socketSend (int hSocket, char * Rqst, short lenRqst)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 200;
    tv.tv_usec = 20;

    if (setsockopt(hSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(tv)) < 0)
    {
        cout << "Time Out" << endl;
        return -1;
    }

    shortRetval = send(hSocket, Rqst, lenRqst, 0);

    return shortRetval;
}


int socketReceive (int hSocket, char * Rsp, short RvcSize)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 200;
    tv.tv_usec = 20;

    if (setsockopt(hSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv)) < 0)
    {
        cout << "Time out" << endl;
        return -1;
    }

    shortRetval = recv(hSocket, Rsp, RvcSize, 0);
    cout << "Responese: " << Rsp << endl;

    return shortRetval;
}
int main (void)
{
    int hSocket = 0, read_size = 0;
    struct sockaddr_in server;
    char server_reply[200] = {0};
    char sendToServer[100] = {0};
    
    /* create socket */
    hSocket = SocketCreate();

    if (hSocket == -1)
    {
        cout << "Could not create socket" << endl;
        return 1;
    }
    cout << "Socket created" << endl;

    /* connect */
    if (socketConnect(hSocket) < 0)
    {
        /* pirnt error message */
        cout << "Connect failed" << endl;
        return 1;
    }
    cout << "Connect done!" << endl;
    cout << "Successfully connected with server" << endl;
    cout << "Enter the message" << endl;
    cin.getline(sendToServer, 100);

    /* Send data to the server */
    socketSend(hSocket, sendToServer, strlen(sendToServer)); 

    read_size = socketReceive(hSocket, server_reply, 200);
    cout << "Server Response: " << server_reply << endl;

    close(hSocket);

    return 0;
}
