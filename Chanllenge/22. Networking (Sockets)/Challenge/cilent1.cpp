#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

short SocketCreate (void)
{
    short hsocket;
    cout << "Create the socket" << endl;
    hsocket = socket(AF_INET, SOCK_STREAM, 0);

    return hsocket;
}

/* try to connect with server */
int SocketConnect (int hsocket)
{
    int iretval = -1;
    int serverport = 9050;

    struct sockaddr_in remote = {0};
    remote.sin_addr.s_addr = inet_addr("127.0.0.1");
    remote.sin_family      = AF_INET;
    remote.sin_port        = htons(serverport);

    iretval = connect(hsocket, (struct sockaddr *)&remote, sizeof(remote));

    return iretval;
}

/* send information */
int SocketSend (int hsocket, char *rqst, short lenrqst)
{
    int retval = -1;
    struct timeval tv;
    tv.tv_sec = 200;
    tv.tv_usec = 20;

    if (setsockopt(hsocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(tv)) < 0)
    {
        cout << "Time out" << endl;
        return -1;
    }

    retval = send(hsocket, rqst, lenrqst, 0);

    return retval;
}


/* receive information */
int SocketReceive (int hsocket, char * rsp, int rcvsize)
{
    int retval = -1;
    struct timeval tv;
    tv.tv_sec = 200;
    tv.tv_usec = 20;

    if (setsockopt(hsocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(tv)) < 0)
    {
        cout << "Time out" << endl;
        return -1;
    }

    retval = recv(hsocket, rsp, rcvsize, 0);

    cout << "Respone: " << rsp << endl;

    return retval;
}


int main (void)
{
    int hsocket = 0, read_size = 0;
    struct sockaddr_in server;
    char sendmessage[200];

    /* create socket */
    hsocket = SocketCreate();
    if (hsocket == -1)
    {
        cout << "Counl'nt create socket" << endl;
        return -1;
    }

    /* connect */
    if (SocketConnect(hsocket) < 0)
    {
        cout << "Connect failed!" << endl;
        return 1;
    }
    cout << "Connect done!" << endl;
    cout << "Successfully connected with server!" << endl;
    cout << "Enter the message" << endl;
    cin.getline(sendmessage, 200);

    /* send data to server */
    SocketSend(hsocket, sendmessage, strlen(sendmessage));

    close(hsocket);

    return 0;
}
