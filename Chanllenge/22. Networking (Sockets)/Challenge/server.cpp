/* 
 * Write threee program for this challenge
 * 
 * two fo the programs will be cilent programs
 * 
 * the functionality of the programs should be as follows
 * 
 *      cilent1 will send an integer to the server process
 *      the server will decrement the number and send the result to cilent2
 *      the server should print both the value it receives and the value that it sends
 *      cielnt2 prints the number it receives and then all the processes should terminate
 * 
 * the server should be listening for numbers on a port known to the cilents
 *      should handle the cilent connections sequentially and accept connections from multiple cilents
 * 
 * after servicing one cilent to completion (cilent1), it should proceed to the next
 */

#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

short SocketCreate (void)
{
    short hsocket;
    cout << "Create the socket" << endl;
    hsocket = socket(AF_INET, SOCK_STREAM, 0);

    return hsocket;
}


int BindCreatedSocket (int hsocket)
{
    int iretval = -1;
    int cilentport = 9050;
    struct sockaddr_in remote = {0};

    /* internet address family */
    remote.sin_family = AF_INET;

    /* Any incoming interface */
    remote.sin_addr.s_addr = htonl(INADDR_ANY);
    remote.sin_port        = htons(cilentport);

    iretval = bind(hsocket, (struct sockaddr *)&remote, sizeof(remote));

    return iretval;
}

int main (void)
{
    int socketdescriptor = 0, sock = 0, cilentlen = 0, sock2 = 0;
    struct sockaddr_in cilent;
    struct sockaddr_in cilent2;

    /* create socket */
    if ((socketdescriptor = SocketCreate()) == -1)
    {
        cout << "Could'nt create socket" << endl;
        return 1;
    }
    cout << "Socket created" << endl;
    if (BindCreatedSocket(socketdescriptor) < 0)
    {
        cout << "Bind failed" << endl;
        return 1;
    }
    cout << "Bind done!" << endl;

    /* listen */
    listen(socketdescriptor, 2);

    /* accept incoming connection */

    for (;;)
    {
        cout << "Waiting for incoming connections..." << endl;
        cilentlen = sizeof(struct sockaddr_in);

        /* accept connection from an incoming cilent */
        sock = accept(socketdescriptor, (struct sockaddr *)&cilent, (socklen_t *)&cilentlen);
        sock2 = accept(socketdescriptor, (struct sockaddr *)&cilent2, (socklen_t *)&cilentlen);
        if ((sock < 0) || (sock2 < 0))
        {
            cout << "accept failed!" << endl;
            return 1;
        }
        char cilentmessage[200];
        cout << "Connection accepted" << endl;
        if (recv(sock, cilentmessage, 200, 0) < 0)
        {
            cout << "receive failed!" << endl;
            break;
        }
        int temp = atoi(cilentmessage);
        if (temp == 0)         // neu reply khong phai chu so
        {
            cout << "cilent reply is'nt digit" << endl;
            cout << "End program" << endl;
            return 1;
        }
        cout << "Receive form cilent1: " << temp << endl;
        --temp;
        sprintf(cilentmessage, "%d", temp);


        /* send some data */
        if (send(sock2, cilentmessage, 200, 0) < 0)
        {
            cout << "Send failed" << endl;
            return 1;
        }

        /* receive from cilent2 */
        char s[1];
        if (recv(sock2, s, 1, 0) < 0)
        {
            cout << "receive failed!" << endl;
            break;
        }
        if (*s == 'y')
        {
            close(sock);
            close(sock2);
            cout << "End program" << endl;
            break;
        }
    }

    return 0;
}
