/* 
 * Write a program in which multiple threads print a message
 * create 10 separate threads and have each thread call one function that takes an argument
 *      pass a different number for each thread to your thread function
 *      might want to use an array to store each thread and to store each number
 * create a global variable named conter and initialize it to 0 (shared variable amongst threads)
 * 
 * create one function that accepts one parameter (void *)
 *      function should get invoked for each thread that you create
 *      function should increment the global counter
 *      function should then print out the message passed into it, its thread id and the global counter
 *      function should print out again the message passed into it, its thread id, and the global counter
 * lastlt, the main thread should call join and exit on all if its child threads
 *      to make the original thread wait for its child thread to complete
 * run the program many times, did you notice anything strange about the global counter values for the two
 * statement in the same thread ?
 */

#include <iostream>
#include <pthread.h>
#include <unistd.h>

#define NTHREAD     10

using namespace std;

void * func_call (void *);

int counter = 0;

int main (void)
{
    pthread_t th[NTHREAD];
    int num[NTHREAD];

    for (int i = 0; i < NTHREAD; ++i)
    {
        num[i] = i;
        pthread_create(th + i, NULL, func_call, (void *)(num + i));
    }

    for (int i = 0; i < NTHREAD; ++i)
    {
        pthread_join(th[i], NULL);
    }

    return 0;
}

void * func_call (void *x)
{
    int *b = (int *)x;
    ++counter;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " modified the counter to " << counter << endl;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " read the counter " << counter << endl;
    pthread_exit(NULL);
}