/*
 * edit code form challenge 1 use mutex
 */


#include <iostream>
#include <pthread.h>
#include <unistd.h>

#define NTHREAD     10

using namespace std;

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void * func_call (void *);

int counter = 0;

int main (void)
{
    pthread_t th[NTHREAD];
    int num[NTHREAD];

    for (int i = 0; i < NTHREAD; ++i)
    {
        num[i] = i;
        pthread_create(th + i, NULL, func_call, (void *)(num + i));
    }

    for (int i = 0; i < NTHREAD; ++i)
    {
        pthread_join(th[i], NULL);
    }

    return 0;
}

void * func_call (void *x)
{
    pthread_mutex_lock(&m);
    int *b = (int *)x;
    ++counter;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " modified the counter to " << counter << endl;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " read the counter " << counter << endl;
    pthread_mutex_unlock(&m);
    pthread_exit(NULL);
}