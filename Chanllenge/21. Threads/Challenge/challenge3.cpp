/* 
 * test your understanding of threads and condition variables
 * we will modify the program form challenge 2 so that certain threads execute critical setions of code before other threads do
 *
 * this program sould print messages from threads that pass in an even number first (parameter) and those that pass
 * in an odd number after all the evens have printed
 *      since we are not guaranteed that the threads will start in any given order, we must have the odd threads wait until all
 *      the even threads have printed, ew don't care about the oder that they print their message
 * 
 * this program will require you to use condition variables to accomplish the ordering
 *      all of the odd threads will sleep until a certain condition is met (all the even threads have finished)
 * 
 * condition variable are always associated with locks because the shared information that they
 * depend on must be sychronized across threads
 */


#include <iostream>
#include <pthread.h>
#include <unistd.h>

#define NTHREAD 20

using namespace std;

pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock        = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t lock_cond    = PTHREAD_COND_INITIALIZER;

void * func_call (void *);

int counter = 0;

int even_number = 0;

int main (void)
{
    pthread_t th[NTHREAD];
    int num[NTHREAD];
    for (int j = 0; j < 1; ++j)
    {
        for (int i = 0; i < NTHREAD; ++i)
        {
            num[i] = i;
            pthread_create(th + i, NULL, func_call, (void *)(num + i));
        }

        sleep(1);
        for (;;)
        {
            if (even_number == (NTHREAD / 2))
                {
                    pthread_cond_broadcast(&lock_cond);
                    break;
                }
        }
        for (int i = 0; i < NTHREAD; ++i)
        {
            pthread_join(th[i], NULL);
        }
        counter = 0;
        even_number = 0;
    }
    return 0;
}

void * func_call (void *x)
{
    pthread_mutex_lock(&lock);
    int *b = (int *)x;

    if ((*b % 2) == 0)
    {
        ++even_number;
    }
    else
    {
        pthread_cond_wait(&lock_cond, &lock);
    }
    pthread_mutex_lock(&count_mutex);
    ++counter;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " modified the counter to " << counter << endl;
    cout << "Message is " << *b << ", thread id = " << pthread_self() << " read the counter " << counter << endl;
    pthread_mutex_unlock(&count_mutex);
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}