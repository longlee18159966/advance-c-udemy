#include <iostream>
#include <pthread.h>

using namespace std;

void * hello_fun (void *)
{
    cout << "Hello World" << endl;
    return NULL;
}

int main (int argc, char *argv[])
{
    pthread_t thread = 0;
    pthread_create(&thread, NULL, hello_fun, NULL);
    pthread_join(thread, NULL);

    return 0;
}