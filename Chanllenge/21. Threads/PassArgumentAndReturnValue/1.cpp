#include <iostream>
#include <pthread.h>

using namespace std;

void *print_message_function (void *x)
{
    char *s = (char *) x;
    cout << s << endl;
}

int main (void)
{
    pthread_t thread1, thread2;

    const char *ms1 = "Thread1";
    const char *ms2 = "Thread2";
    int iret1, iret2;

    iret1 = pthread_create(&thread1, NULL, print_message_function, (void*) ms1);
    iret2 = pthread_create(&thread2, NULL, print_message_function, (void*) ms2);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    cout << "Thread 1 return: " << iret1 << endl;
    cout << "Thread 2 return: " << iret2 << endl;
    pthread_exit(NULL);
    exit(0);
}