#include <iostream>
#include <pthread.h>
#include <malloc.h>

using namespace std;

typedef struct 
{
    char *s;
    int i;
    float f;
} test;

void *print_message_function (void *x)
{
    test *a = (test *)x;
    cout << a->i << endl;
    cout << a->f << endl;
    cout << a->s << endl;
    free(a->s);
    free(a);
}

int main (void)
{
    test *q = new test;
    pthread_t thread1;
    int iret1;
    q->i = 1;
    q->f = 34;
    q->s = "Hello World";


    iret1 = pthread_create(&thread1, NULL, print_message_function, (void*) q);

    pthread_join(thread1, NULL);

    cout << "Thread 1 return: " << iret1 << endl;
    pthread_exit(NULL);
    exit(0);
}