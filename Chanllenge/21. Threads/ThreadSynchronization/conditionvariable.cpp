#include <iostream>
#include <pthread.h>
#include <stdlib.h>

#define COUNT_DONE  10
#define COUNT_HALT1 3
#define COUNT_HALT2 6

using namespace std;

pthread_mutex_t count_mutex         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t condition_mutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_cond       = PTHREAD_COND_INITIALIZER;

void * functionCount1 (void *);
void * functionCount2 (void *);

int count = 0;

int main (void)
{
    pthread_t th1, th2;

    pthread_create(&th1, NULL, functionCount1, NULL);
    pthread_create(&th2, NULL, functionCount2, NULL);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);

    exit(0);
}

void * functionCount1 (void *x)
{
    for (;;)
    {
        pthread_mutex_lock(&condition_mutex);

        while ((count >= COUNT_HALT1) && (count <= COUNT_HALT2))
        {
            pthread_cond_wait(&condition_cond, &condition_mutex);
        }

        pthread_mutex_unlock(&condition_mutex);

        pthread_mutex_lock(&count_mutex);
        ++count;
        cout << "Counter value functionCount1: " << count << endl;
        pthread_mutex_unlock(&count_mutex);

        if (count >= COUNT_DONE)
            return NULL;
    }
}

void * functionCount2 (void *x)
{
    for (;;)
    {
        pthread_mutex_lock(&condition_mutex);

        if ((count < COUNT_HALT1) || (count > COUNT_HALT2))
        {
            pthread_cond_signal(&condition_cond);
        }

        pthread_mutex_unlock(&condition_mutex);

        pthread_mutex_lock(&count_mutex);
        ++count;
        cout << "Counter value functionCount2: " << count << endl;
        pthread_mutex_unlock(&count_mutex);

        if (count >= COUNT_DONE)
            return NULL;
    }
}

