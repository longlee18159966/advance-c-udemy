#include <iostream>
#include <unistd.h>
#include <pthread.h>

#define RESOLVE

using namespace std;

void * source1 (void *);
void * source2 (void *);

static pthread_mutex_t m1 = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t m2 = PTHREAD_MUTEX_INITIALIZER;

int main (int argc, char *argv[])
{
    pthread_t t1, t2;

    pthread_create(&t1, NULL, source1, NULL);
    pthread_create(&t2, NULL, source2, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    return 0;
}

void * source1 (void *x)
{
    cout << "Start form source 1" << endl;
    sleep(1);
    pthread_mutex_lock(&m1);
    cout << "Get source 2" << endl;

    #ifdef PROBLEM
    pthread_mutex_lock(&m2);
    #endif

    #ifdef RESOLVE
    while (pthread_mutex_trylock(&m2))
    {
        pthread_mutex_unlock(&m1);
        sleep(1);
        pthread_mutex_lock(&m1);
    }
    #endif

    cout << "Aquired source 2" << endl;
    pthread_mutex_unlock(&m2);
    cout << "Finish source 1" << endl;
    pthread_mutex_unlock(&m1);
    pthread_exit(NULL);
}


void * source2 (void *)
{
    cout << "Start form source 2" << endl;
    sleep(1);
    pthread_mutex_lock(&m2);
    cout << "Get source 1" << endl;

    #ifdef PROBLEM
    pthread_mutex_lock(&m1);
    #endif

    #ifdef RESOLVE
    while (pthread_mutex_trylock(&m1))
    {
        pthread_mutex_unlock(&m2);
        sleep(1);
        pthread_mutex_lock(&m2);
    }
    #endif

    cout << "Aquired source 1" << endl;
    pthread_mutex_unlock(&m1);
    cout << "Finish source 2" << endl;
    pthread_mutex_unlock(&m2);
    pthread_exit(NULL);
}