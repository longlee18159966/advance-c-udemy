#include <iostream>
#include <pthread.h>
#include <unistd.h>


using namespace std;

static pthread_mutex_t lock = NULL;

int j = 0;

void * do_process (void *)
{
    pthread_mutex_lock(&lock);
    int i = 0;
    ++j;

    while (i < 5)
    {
        cout << j << endl;
        sleep(1);
        ++i;
    }
    cout << "...Done!" << endl;
    pthread_mutex_unlock(&lock);
}

int main (void)
{
    int err = 0;
    pthread_t th1, th2;

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        cout << "Mutex initialization failed " << endl;
        return 1;
    }

    j = 0;

    pthread_create(&th1, NULL, do_process, NULL);
    pthread_create(&th2, NULL, do_process, NULL);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);

    return 0;
}