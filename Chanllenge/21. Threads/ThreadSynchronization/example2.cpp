#include <iostream>
#include <pthread.h>
#include <unistd.h>

#define NTHREAD 10

using namespace std;

static pthread_mutex_t x = PTHREAD_MUTEX_INITIALIZER;

int counter = 0;

void * func_call (void *)
{
    cout << "Thread number: " << pthread_self() << endl;
    pthread_mutex_lock(&x);
    ++counter;
    pthread_mutex_unlock(&x);
}

int main (void)
{
    pthread_t thread[NTHREAD];

    for (int i = 0; i < NTHREAD; ++i)
        pthread_create(thread + i, NULL, func_call, NULL);

    for (int i = 0; i < NTHREAD; ++i)
        pthread_join(*(thread + i), NULL);
    cout << "Final counter is: " << counter;

    return 0;
}