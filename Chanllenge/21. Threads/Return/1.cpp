#include <iostream>
#include <pthread.h>

using namespace std;

typedef struct
{
    char *s;
    int i;
    float f;
} test;

void * ret_fun (void *x)
{
    test *a = new test;
    a = (test *)x;
    a->i += 3;
    a->f += 4;

    return (void *)a;
}

int main (void)
{
    pthread_t thread;
    test *x = new test;
    x->i = 1;
    x->f = 23;
    x->s = "hello";
    test *ret = new test;

    pthread_create(&thread, NULL, ret_fun, (void *)x);
    pthread_join(thread, (void **) &ret);
    cout << ret->i << '\t' << ret->f << endl << ret->s;  /* note */
    pthread_exit(NULL);

    return 0;
}