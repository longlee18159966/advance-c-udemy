#include <iostream>
#include <pthread.h>
#include <unistd.h>

using namespace std;

void * threadfn (void *arg)
{
    cout << "Before" << endl;
    pthread_detach(pthread_self());
    cout << "Thread fn" << endl;
    pthread_exit(NULL);
}

int main (void)
{
    pthread_t tid;
    int ret = pthread_create(&tid, NULL, threadfn, NULL);

    if (ret != 0)
    {
        perror("Thread creation error");
        exit(1);
    }

    cout << "After thread created in main" << endl;
    pthread_exit(NULL);
}