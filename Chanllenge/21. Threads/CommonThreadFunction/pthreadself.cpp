#include <iostream>
#include <pthread.h>

using namespace std;

/* pthread_self() : get id of thread */

void * calls (void *ptr)
{
    cout << "In function" << endl << "Thread id = " << pthread_self();
    pthread_exit(NULL);
    return NULL;
}

int main (void)
{
    pthread_t thread;
    pthread_create(&thread, NULL, calls, NULL);

    cout << "In main " << endl << "Thread id = " << thread;
    pthread_join(thread, NULL);
    
    return 0;
}