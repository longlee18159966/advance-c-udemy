

/* 
 * pthread_equal and pthread_one
 * pthread_equal() compares two thread IDs
 *  if the two ID are different 0 is returned, ootherwise a non-zero value is returned
 *  operator == should not be used to compare two thread IDs against each other
 * 
 * pthread_equal(thread1, thread2);
 * 
 * the pthread_once executes the init_routine exactly once in a process
 *  the first call to this by any thread in the process executes the given init_routine without parameters
 *  any subsequent call will have no effect
 * 
 * pthread_once(once_control, int_routine);
 * 
 * pthread_cancle();
 */

#include <iostream>
#include <pthread.h>
#include <unistd.h>

using namespace std;

void * my_func (void *ptr)
{
    cout << "Hello" << endl;
    pthread_cancel(pthread_self());
    cout << "After" << endl;
    return NULL;
}

int main (void)
{
    pthread_t thread;

    pthread_create(&thread, NULL, my_func, NULL);

    pthread_join(thread, NULL);

    return 0;
}