#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <iomanip>

using namespace std;

pthread_attr_t attr;

void *dowork(void *threadid)
{
    long *tid;
    size_t mystacksize;

    tid = (long*) threadid;
    pthread_attr_getstacksize(&attr, &mystacksize);

    cout << "Thread " << *tid << ": stack size = " << mystacksize << " bytes" << endl;
    pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
    pthread_t mythread;
    size_t stacksize;
    pthread_t myID;
    long t = 5;

    pthread_attr_init(&attr);
    pthread_attr_getstacksize(&attr, &stacksize);
    cout << "Default stack size = " << setbase(10) << stacksize << endl;
    stacksize = 9000000; //(9MB);
    cout << "Amount of stack needed per thread = " << stacksize << endl;
    pthread_attr_setstacksize(&attr, stacksize);

    cout << "Create thread with stack size = " << stacksize << " bytes" << endl;

    myID = pthread_create(&mythread, &attr, dowork, (void *)&t);

    if (myID != 0)
    {
        cout << "ERROR! Return code form pthread_reate() is " << myID << endl;
        exit(-1);
    }

    pthread_exit(NULL);
}