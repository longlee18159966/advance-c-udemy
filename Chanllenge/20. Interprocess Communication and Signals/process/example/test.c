#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main (void)
{
    int mypid, myppid;
    mypid = getpid();
    myppid = getpid();
    printf("mypid = %d\n", mypid);
    printf("myppid = %d\n", myppid);

    system("ps -ef");

    return 0;
}