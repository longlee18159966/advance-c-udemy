TIẾN TRÌNH:

- tiến trình là một chương trình đang được thực thi

- một chương trình là một file chứa các thông tin về tiến trinhf
    - khi chương trình được bắt đầu, nó được tải vào RAM và bắt đầu thực thi

- mỗi một tiến trình đều có không gian địa chỉ riêng của nó và thường có một luồng (thread) điều khiển
- mỗi một tiến trình có một vùng nhớ riêng và chỉ tiến trình đó mới có thể "nhìn thấy" vùng nhớ đó

- có thể xảy ra trường hợp nhiều tiến trình cùng thực thi một chương trình:
    - tuy nhiên mỗi tiến trình đều có một bản sao của chương trình bên trong vùng không gian địa chỉ của nó và 
      thực thi độc lập với các bản sao khác

- tiến trình được tổ chức theo cấu trúc phân tầng
    - mỗi tiến trình có một tiến trình cha, tiến trình cha được sắp xếp để tạo ra nó (tiến trình con)

- tiến trình được tạo bởi tiến trình cha được gọi là tiến trình con
    - tiến trình con kế thừa nhiều thuộc tính (có thể là tất cả) từ tiến trình cha của nó

- system call là một yêu cầu cần thực hiện mà chương trình gửi tới kernel
    - yêu cầu thực hiện đó chỉ có thể thực hiện bởi kernel do kernel được cấp quyền ưu tiên
    - system call còn được gọi là kernel calls

- mỗi một tiến trình được xác định bởi một số dương DUY NHẤT được gọi là process ID (PID) (cái này tương tự mã sinh viên)

- system call getpid() trả về PID của process đang gọi
    - system call này luôn thực thi thành công và không trả về giá trị

GIAO TIẾP GIỮA CÁC TIẾN TRÌNH

- một tiến trình có thể thuộc 1 trong 2 loại sau:
    - tiến trình độc lập
        - không bị ảnh hưởng bởi việc thực thi tiến trình khác
        - không giao tiêp với các chương trình khác
    - tiến trình phối hợp
        - bị ảnh hưởng bởi việc thực thi tiến trình khác
        - có thể được tăng hiệu suất tính toán, cho thuận tiện hoặc để chia module
- giao tiếp giữa các tiến trình là cho phép tiến trình làm việc cùng nhau
    - thường sẽ giúp chương trình power full hơn
    - sử dụng kỹ thuật gọi là IPC 

- do mỗi tiến trình đều có vùng địa chỉ riêng, nếu một tiến trình muốn giao tiếp, chia sẻ thông tin 
  từ không gian địa chỉ của nó tới các tiến trình khác, cần sử dụng kỹ thuật IPC (inter process communication)

- việc giao tiếp, trao đổi thông tin giữa các tiến trình có thể coi như một phương pháp 
  phối hợp làm việc giữa chúng

- giao tiếp giữa các tiến trình chia làm 2 dạng:
    - giữa các tiến trình có liên quan tới nhau, được tạo ra từ một tiến trình duy nhất (như là tiến trình cha và tiến trình con)
    - giữa các tiến trình không liên quan hoặc 2 tiến trình khác nhau 

- một tiến trình có thể giao tiếp với tiến trình khác bằng nhiều cách khác nhau:
    - pipes (cùng một tiến trình, cùng chương trình)
        - tiến trình đầu tiên giao tiếp với tiến trình thứ 2, cho phép dữ liệu đi theo 1 hướng (half duplex - bán song công)
    - named pipes (giữa các tiến trình khác nhau, FIFO)
        - tiến trình thứ nhất có thể giao tiếp với tiến trình thứ 2, cho phép dữ liệu đi 
          theo 2 hướng cùng một lúc (full duplex - song công)
    - message queues
        - tiến trình sẽ giao tiếp với các tiến trình khác bằng cách gửi một message và lấy nó ra khỏi queues (song công)
    - shared memory
        - có thể giao tiếp giữa 2 hoặc nhiều tiến trình bằng cách chia sẻ một phần bộ nhớ giữa các tiến trình
    - sockets
        - thường được sử dụng trong giao tiếp mạng, giữa máy chủ và khách
    - signals
        - giao tiếp giữa nhiều tiến trình thông qua tín hiệu
        - tiến trình gốc sẽ gửi tín hiệu (được phát hiện dưới dạng số) và tiến trình đích sẽ xử lý nó
    
PIPES

- pipes là cơ chế giao tiếp giữa 2 hay nhiều tiến trình có liên quan hoặc hoàn toàn độc lập
    - có thể dùng trong 1 tiến trình hoặc giao tiếp giữa tiến trình con và tiến trình cha

- có thể giao tiếp ở nhiều tầng tiến trình, tiến trình cụ, ông bà giao tiếp với tiến trình cháu chắt chút chít chụt chịt :v

- pipes còn được gọi là FIFO

MESSAGE QUEUES

- tất cả các tiến trình có thể trao đổi thông tiên thông qua việc truy cập message queues của hệ thống

- message queues là sự pha trộn giữa signals và sockets
    - cho phép tạo dòng dữ liệu kết nối với nhiều tiến trình
    - việc gửi và nhận được thực hiện bởi một hàm đơn giản

- tiến trình gửi gửi một message vào trong queues, tiến trình khác có thể đọc nó (đọc message)
    - mỗi một message đều có một định danh hoặc kiểu xác định giúp tiến trình lựa chọn message phù hợp
    - các tiến trình phải chia sẻ key chung để truy cập vào queues lần đầu tiên

SHARED MEMORY

- giao tiếp được thực hiện trong vùng bộ nhớ chung, khi một tiến trình thay đổi vùng nhớ đó, 
  các tiến trình khác có thể nhận biết sự thay đổi đó

- việc giao tiếp giữa tiến trình bắt buộc tiến trình đó phải chia sẻ 1 số biến, việc này được quyết định bởi programmer






    

