#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

void signalHandler (int signum);

int main (void)
{
    int i = 0;  /* counter used to loop 100 times */
    int x = 0; /* variable to hold random values between 1-50 */
    signal(SIGINT, signalHandler);
    srand(clock());

    for (i = 1; i <= 1000; ++i)
    {
        x = 1 + rand() % 50;    /*  generate random number to raise signal */
        if (x == 25)            /*  raise signal SIGINT when x = 25 */
        {
            raise(SIGINT);
        }
        printf("%4d", i);
        if ((i % 10) == 0)
        {
            putchar('\n');
        }
    }

    return 0;
}

void signalHandler (int signum)
{
    int response;

    printf("\n%s%d%s\n", "\nInterrupt signal ( ", signum, " ) received.");
    printf("\nDo you wish to continue (1 to yes or 2 to no)  ");
    scanf("%d", &response);
    while ((response != 1) && (response != 2))
    {
        printf("(1 to yes or 2 to no)?\n");
        scanf("%d", &response);
    }
    if (response == 1)
        signal(SIGINT, signalHandler);
    else
        exit(EXIT_SUCCESS);
}
