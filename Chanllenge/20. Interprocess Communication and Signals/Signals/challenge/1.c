
/*  
 *  Specifically, program will:
 *      raise signals
 *      catch signals
 *      use the alarm function to raise a signal
 */

/* 
 * Write a program that will test a user's multiplication skills
 *  the program will ask the user to work on an answer to a simple multplication program
 *  kepp track of how many answer are correct
 *
 * The program will keep runnig forever, unless
 *  the user presses Ctrl-C OR
 *  the user take more than 5 seconds to answer the question
 *
 * Program should display final result (correct answer)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

static int score = 0;

void end_game (void);
void error (char *msg);
void handler (int signum);
void process_signal (void);


int main (void)
{
    srand(time(0));

    for (;;)
    {
        int a = rand() % 11;
        int b = rand() % 11;
        char txt[4];
        
        printf("\nYou have 5 second to answer\n");
        printf("\nWhat is %d times %d: ", a, b);
        process_signal();
        fgets(txt, 4, stdin);

        int answer = atoi(txt);

        if (answer == (a * b))
            ++score;
        else
            printf("\nWrong! Score: %d\n", score);
    }

    return 0;
}



void end_game (void)
{
    printf("\nFinal score: %d\n\n", score);
    exit(0);
}



void error (char *msg)
{
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
}



void handler (int signum)
{
    if (signum == SIGINT)
    {
        printf("\nTerminate!\n");
        end_game();
    }
    else if (signum == SIGALRM)
    {
        printf("\n\nTime's up!\n");
        end_game();
    }
}



void process_signal (void)
{
    void (*sighandlerReturn) (int);
    alarm(4);
        if ((sighandlerReturn = signal(SIGINT, handler)) == SIG_ERR)
        {
            perror("Signal Error:\n");
            return ;
        }
        if ((sighandlerReturn = signal(SIGALRM, handler)) == SIG_ERR)
        {
            perror("Signal Error:\n");
            return ;
        }
}




















