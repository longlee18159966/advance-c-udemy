#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX_COUNT 50

void child (void);
void parent (void);

int main (void)
{
    pid_t pid;

    pid = fork();

    if (pid == 0)
        child();
    else if (pid > 0)
        parent();
    else
        printf("fork() failed\n");
    return 1;
}

void child (void)
{
    int i;
    for (i = 1; i < MAX_COUNT; ++i)
        printf("This line is from child, value = %d\n", i);
    printf("*** child process is done ***\n");
}

void parent (void)
{
    int i;
    for (i = 1; i < MAX_COUNT; ++i)
        printf("This line is from parent, value = %d\n", i);
    printf("*** parent process is done ***\n");
}
