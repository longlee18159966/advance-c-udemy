
/* 
 * Write a program to create one parent with three children process (four total process)
 *  must use the fork() system call
 *
 *  Program should contain output that identifies each parent and each child
 *      will need to write if statements to check process id's returned from fork() call
 *      , so that the output information is correct
 *      "parent", "1st child", "2nd child", "3rd child" 
 *      utilize the getpid() and getppid() functions to display each processes id
 */


#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

void parent (void);
void firstchild (void);

int main (void)
{
    pid_t pid;

    pid = fork();

    if (pid == 0)
        firstchild();
    else if (pid > 0)
        parent();
    else 
    {
        printf("\nfork() failed\n");
        return 1;
    }
}


void parent (void)
{
    printf("\nparent\n");
    printf("parent id: %d\n", getpid());
    printf("my parent parent id: %d\n", getppid());
    sleep(2);
}

void thirdchild (void)
{
    printf("\nthird child\n");
    printf("third id: %d\n", getpid());
    printf("my third's parent id: %d\n", getppid());
}

void secondchild (void)
{
    pid_t pid;
    pid = fork();

    if (pid == 0)
    {
        printf("\nsecond child\n");
        printf("second id: %d\n", getpid());
        printf("my second's parent id: %d\n", getppid());
    }
    else if (pid > 0)
    {
        thirdchild();
    }
    else
    {
        printf("\nfork() failed\n");
        return ;
    }
}

void firstchild (void)
{
    pid_t pid;
    pid = fork();

    if (pid == 0)
    {
        printf("\nfirst child\n");
        printf("first id: %d\n", getpid());
        printf("my first's parent id: %d\n", getppid());
    }
    else if (pid > 0)
    {
        secondchild();
    }
    else
    {
        printf("\nfork() failed\n");
        return ;
    }
}
