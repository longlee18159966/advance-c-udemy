/* 
 * Write a program that takes as input, a set of numbers from a file and write 
 * even, odd and prime numbers to standard output
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool isprime (int number);

int main (int argc, char *argv[])
{
	if (argc != 2)
	{
		puts("Syntax error!");
		fprintf(stderr, "Use %s filename\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	FILE *source;
	char buffer[256];
	int number;
	if ((source = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "Can't open file %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}	
	while (fscanf(source, "%d", &number) != 0)
	{
		/* check even number */
		if ((number % 2) == 0)
		{
			printf("Even number found: %d\n", number);
		}
		/* check odd number */
		else if ((number % 2) == 1)
		{
			/* check prime number */
			if (isprime(number))
			{
				printf("Prime number found: %d\n", number);
			}
			else
			{
				printf("Odd number found: %d\n", number);
			}
		}
		

		/* check for end-of-file */
		if (feof(source))
		{
			break;
		}
	}

	fclose(source);
	return 0;
}

bool isprime (int number)
{
	if (number < 2)
	{
		return false;
	}

	for (int i = 2; i <= number / 2; ++i)
	{
		if ((number % i) == 0)
		{
			return false;
		}
	}
	return true;
}