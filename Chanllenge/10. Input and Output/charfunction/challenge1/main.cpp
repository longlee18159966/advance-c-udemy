/* 
 * Write a program in C to count the number of words and characters in a file OR from standard input
 * this program can take zero command-line arguments or one command-line argument
 * if there is one argument, it is interpreted as the name of a file
 * if there is no argument, the standard input (stdin) is to be used for input 
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctype.h>

using namespace std;

int main (int argc, char *argv[])
{
    if (argc == 1)
    {
        /* Read from stdin */
        char ch;
        int word = 1;
        int character = 0;
        bool in_word = true;
        cout << "Read from stdin" << endl;
        while ((ch = getchar()) != '#')
        {
            ++character;
            if (isalnum(ch) || ispunct(ch))
            {
                if (in_word == false)
                {
                    ++word;
                }
                in_word = true;
            }
            else
            {
                in_word = false;
            }
        }
        cout << "Character: " << character << endl;
        cout << "Word: " << word << endl;
    }
    else if (argc == 2)
    {
        /* Read from file */
        FILE *fp;
        if ((fp = fopen(argv[1], "r")) == NULL)
        {   
            fprintf(stderr, "Cannot open file. File doesn't exist");
            exit(EXIT_FAILURE);
        }
        char ch;
        int word = 1;
        int character = 0;
        bool in_word = true;
        cout << "Read from file" << endl;
        while ((ch = fgetc(fp)) != '#')
        {
            ++character;
            if (isalnum(ch) || ispunct(ch))
            {
                if (in_word == false)
                {
                    ++word;
                }
                in_word = true;
            }
            else
            {
                in_word = false;
            }
        }
        cout << "Character: " << character << endl;
        cout << "Word: " << word << endl;
        fclose(fp);
    }
    else
    {
        fprintf(stderr, "Syntax error\nUsing %s\nor\n%s \"file name\"", argv[0], argv[0]);
    }



    return 0;
}