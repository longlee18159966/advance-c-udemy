/* 
 * Write a C program to convert uppercase to lowercase and vice versa in file
 * this program can take a command-line argument for the name of the file or 
 * you can ask the user for the name of the file
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
 

using namespace std;

int main (int argc, char *argv[])
{
    FILE *source;
    FILE *target;
    char ch;
    if (argc == 1)
    {
        /* Ask user for the name of the file */
        char file_name[40];
        cout << "Enter the file name" << endl;
        cin >> file_name;
        if ((source = fopen(file_name, "r")) == NULL)
        {
            cout << "Can't open file: " << file_name << "\nExit" << endl;        
            exit(EXIT_FAILURE);
        }
        target = fopen("temporary", "w+");
        while ((ch = fgetc(source)) != EOF)
        {
            if ((ch >= 'A') && (ch <= 'Z'))
            {
                ch = ch + ('a' - 'A');
            }
            fputc(ch, target);
        }
        fclose(source);
        rewind(target);
        source = fopen(file_name, "w");
        while ((ch = fgetc(target)) != EOF)
        {
            fputc(ch, source);
        }

    }
    else if (argc == 2)
    {
        /* file name from command-line */
        if ((source = fopen(argv[1], "r")) == NULL)
        {
            cout << "Can't open file: " << argv[1] << "\nExit" << endl;        
            exit(EXIT_FAILURE);
        }
        target = fopen("temporary", "w+");
        while ((ch = fgetc(source)) != EOF)
        {
            if ((ch >= 'A') && (ch <= 'Z'))
            {
                ch = ch + ('a' - 'A');
            }
            fputc(ch, target);
        }
        fclose(source);
        rewind(target);
        source = fopen(argv[1], "w");
        while ((ch = fgetc(target)) != EOF)
        {
            fputc(ch, source);
        }
    }
    else
    {
        cout << "Syntax error\n" << "Use " << argv[0] << "<file name>" << endl;
        exit(EXIT_FAILURE);
    }

    return 0;
}