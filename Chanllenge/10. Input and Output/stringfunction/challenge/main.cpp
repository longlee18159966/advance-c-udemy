/* 
 * Write a program that takes two command-line arguments
 * the first is a character
 * the seond is a filename
 * the requirements of the program are that it should print only those lines
 * in the file containing the given character
 * line in a file are identified by a terminating '\n'
 * assume that no line is more than 256 characters long
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace std;

int main (int argc, char *argv[])
{
	if (argc != 3)
	{
		cout << "Syntax error!" << "\nUse " << argv[0] << " <character> <filename>" << endl;
		exit(EXIT_FAILURE);
	}
	char buffer[256];
	FILE *source;
	if ((source = fopen(argv[2], "r")) == NULL)
	{
		cout << "Can't open file " << argv[2] << ".\nFile doesn't exist" << endl;
		exit(EXIT_FAILURE);
	}

	while (fgets(buffer, 256, source) != NULL)
	{
		if (buffer[0] == '\n')
		{
			continue;
		}
		char *p;
		if ((p = strchr(buffer, '\n')) != NULL)
		{
			*p = '\0';
		}
		if (strchr(buffer, *argv[1]))
		{
			puts(buffer);
		}
	}
	fclose(source);

	return 0;
}