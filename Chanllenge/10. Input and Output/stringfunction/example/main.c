#define _GNU_SOURCE
#define _POSIX_C_SOURCE >= 200809L

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main (void)
	char *buffer;
	size_t bufsize = 32;
	buffer = (char *)malloc(bufsize * sizeof(char));
	size_t characters;

	printf("Type something\n");
	
	characters = getline(&buffer, &bufsize, stdin);
	printf("%zu characters were read\n", characters);
	printf("You typed: %s\n", buffer);

	return 0;
}