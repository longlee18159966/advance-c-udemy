/* link to read setjmp and longjmp */

/* https://www.geeksforgeeks.org/g-fact22-concept-of-setjump-and-longjump/ */

#include <iostream>
#include <setjmp.h>

using namespace std;



jmp_buf buf;

void func (void)
{
    cout << "Hello" << endl;
    longjmp(buf, 1);
    cout << '2' << endl;
}

int main (void)
{
    if (setjmp(buf))
    {
        cout << '3' << endl;
    }
    else
    {
        cout << '4' << endl;
        func();
    }

    return 0;
}