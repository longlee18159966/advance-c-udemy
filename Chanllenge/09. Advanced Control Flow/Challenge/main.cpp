
/* 
 * Write a C program that uses setjmp and longjmp to gracefully handle unrecoverable program errors
 * When you discover an unreoverable error, you shoud transfer control back to the main input loop and start again from there
 * 
 * Create a function named error_recover that prints out an error and then uses longjmp to transfer control back to a main function loop
 * 
 * Your main function should include a forever loop that uses setjmp at the top of the loop before processing
 * 
 * You can add "dummy" code in your loop that simulates an error (by calling error_recover) when setjmp returns 0
 */

#include <iostream>
#include <setjmp.h>

using namespace std;

jmp_buf buf;

void error_recover (void);

int main (void)
{
    for (;;)
    {
        if (setjmp(buf))
        {
            cout << "Back to main" << endl;
            break;
        }
        else
        {
            error_recover();
        }
    }

    return 0;
}

void error_recover (void)
{
    cout << "Detected unrecoverable error" << endl;
    longjmp(buf, 1);
}

