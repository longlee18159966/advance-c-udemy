#include <stdio.h>
#include <stdlib.h>


int main (void)
{
    int a[5] = {0};
    int *restrict p = (int *) malloc(5 * sizeof(int));
    int *v = a;

    for (int i = 0; i < 5; ++i)
    {
        v[i] += 4;
        p[i] += 4;

        v[i] += 2;
        p[i] += 2;
        /* line 14 and line 17 will be optimize with one line : p[i] += 6
         * but line 13 and line 16 is not
         * because the restrict keyword
         * note we must use c99 or c11 for this keyword and C++ does not support restrict keyword
         */
    }

    return 0;
}