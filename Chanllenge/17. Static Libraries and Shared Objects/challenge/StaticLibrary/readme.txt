1 - compile library files:
    gcc -c StringFunctions.c -o StringFunctions.o

2 - create static library
    ar rcs lib_stringfunction.a StringFunctions.o

3 - compile main program
    gcc -c main.c -o main.o

4 - link the compiled program to the static library
    gcc -o run.exe main.o lib_stringfunction.a

note option -l and -L in step 4 (read more)