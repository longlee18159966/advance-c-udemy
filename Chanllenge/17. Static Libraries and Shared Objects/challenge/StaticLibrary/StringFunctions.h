/* 
 * Contain implementation of various string manipulation functions:
 * +) Find the frequency of a characters in a string
 * +) Remove all the character in a String except Alphabets
 * +) Calculate the length of a string without using strlen
 * +) Concatenate two strings with using strcat
 * +) Find the substring of given string
 */

#ifndef __S_F_H
#define __S_F_H
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <stdio.h>

/* 
 * str - string to search
 * searchCharacter - character to look for
 * return type - int: count for the number of times that character was found
 */
int numberOfCharactersInString (char *str, char searchCharacter);


/* 
 * source - source string
 * return type - int: 0 on success
 */
int removeNonAlphaCharacters (char *source);


/* 
 * source - source string
 * return type - int: length of string
 */
int lengthOfString (char *source);


/* 
 * str1 - string to concatenate to (destiny)
 * str2 - second string to concatenate from (source string)
 * retrun type - int: 0 on success
 */
int strConcat (char *str1, char *str2);


/* 
 * source - string to coppy from
 * destination - second string to coppy to
 * return type - int: 0 on success
 */
int strCopy (char *source, char *destination);


/* 
 * source - source string
 * from - starting index from where you want to get substring
 * n - number of characters to copied in substring
 * target - target string in which you want to store target string
 * return type - int: 0 on success
 */
int substring (char *source, int from, int n, char *target);

























































#endif