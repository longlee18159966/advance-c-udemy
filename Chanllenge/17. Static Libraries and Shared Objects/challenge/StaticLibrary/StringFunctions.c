/* 
 * Contain implementation of various string manipulation functions:
 * +) Find the frequency of a characters in a string
 * +) Remove all the character in a String except Alphabets
 * +) Calculate the length of a string without using strlen
 * +) Concatenate two strings with using strcat
 * +) Find the substring of given string
 */

/* 
 * Steps

 * create "StringFunctions.c" source file that implements all of the functions in header file

 * create a static library (.a) containing your string manipulation functions: lib_stringfunctions.a
 * 
 * create a program that acts as a driver for your string functions
 * - test all of your functions in this file
 * - statically link this program to your static library
 * 
 * compile and run your program
 */

#include "StringFunctions.h"

int numberOfCharactersInString (char *str, char searchCharacter)
{
    printf("\nCall from static library\n");
    int count = 0;
    int i;
    for (i = 0; i < strlen(str); ++i)
    {
        if (str[i] == searchCharacter)
            ++count;
    }

    return count;
}


int removeNonAlphaCharacters (char *source)
{
    printf("\nCall from static library\n");
    int i = 0;
    int count = 0;
    char temp[strlen(source)];
    for (i = 0, count = 0; i < strlen(source); ++i)
    {
        if (isalpha(source[i]) != 0)
            temp[count++] = source[i];
    }

    strcpy(source, temp);
    source[count + 1] = '\0';
    return count;
}


int lengthOfString (char *source)
{
    printf("\nCall from static library\n");
    int i = 0;
    while (source[i] != '\0')
        ++i;

    return i;
}


int strConcat (char *str1, char *str2)
{
    printf("\nCall from static library\n");
    int i, j;
    int temp = strlen(str1);
    for (i = strlen(str1), j = 0; i < strlen(str1) + strlen(str2), j < strlen(str2); ++i, ++j)
    {
        str1[i] = str2[j];
    }

    if (strlen(str1) == (strlen(str2) + temp))
        return 0;
    else 
        return -1;
}
