

/* Write a proggram that includes a function that modifies a pointers values
 * not the value that the pointer is pointing to
 * the actual value of the pointer (the address that the pointer is pointing to)
 */


#include <iostream>
#include <string>
#include <string.h>

using namespace std;

void foo (char **s)
{
	*s = new char[255];
	strcpy(*s, "Hello1\n");
}

int main (void)
{
	char *s = NULL;
	foo(&s);
	cout << s << endl;
	cout << &s << endl;
	delete s;
	cout << s;

	return 0;
}