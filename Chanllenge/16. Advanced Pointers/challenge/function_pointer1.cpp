
/* Write a program that wil perform some arithmetic operation (user entry)
 * on an aray using function pointers
 */

#include <iostream>

using namespace std;

typedef int (*FuntionOperator) (int, int);

int add (int a, int b);
int sub (int a, int b);
int mul (int a, int b);
int div (int a, int b);

void DisplayArray (int *);

void perform (int *, int *, int);

int array1[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
int array2[] = {38, 27, 87, 63, 59, 223, 132, 1, 19, 7};

FuntionOperator func_arr[] = {add, sub, mul, div};
string s[] = {"Add", "Subtract", "Multiply", "Divide", "None"};

void menu (void);


int main (void)
{
	int choice = 0;
	for (;;)
	{
		menu();
		cout << "Enter choice: ";
		fflush(stdin);
		cin >> choice;
		cout << s[choice - 1] << endl;
		if (choice == 5)
			break;
		perform(array1, array2, choice);
	}
	
	return 0;
}


int add (int a, int b)
{
	return a + b;
}
int sub (int a, int b)
{
	return a - b;
}
int mul (int a, int b)
{
	return a * b;
}
int div (int a, int b)
{
	return a / b;
}

void menu (void)
{
	cout << "Which operation do you to perform?" << endl;
	cout << "1. Add" << endl;
	cout << "2. Sub" << endl;
	cout << "3. Multiply" << endl;
	cout << "4. Divide" << endl;
	cout << "5. None" << endl;
}

void DisplayArray (int *a)
{
	for (int i = 0; i < sizeof(a); ++i)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void perform (int *array1, int *array2, int choice)
{
	int *array3 = new int[10];
	DisplayArray(array1);
	DisplayArray(array2);
	for (int i = 0; i < sizeof(array3); ++i)
		array3[i] = (*func_arr[choice - 1])(array1[i], array2[i]);
	DisplayArray(array3);
	delete array3;
}