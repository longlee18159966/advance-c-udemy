#include <iostream>
#include <malloc.h>
#include <string.h>

using namespace std;

void b (char **s)
{
	*s = (char *) malloc(255);
	strcpy(*s, "Hello 2");
}


char * a (char *s)
{
	s = (char *)malloc(255);
	strcpy(s, "Hello 1");
	return s;
}

void foo (int **ptr)
{
	int a = 5;
	*ptr = &a;
	cout << ptr << endl;
}

int main (void)
{
	int *ptr = NULL;
	ptr = (int *) malloc(sizeof(int));
	*ptr = 10;
	foo(&ptr);
	cout << *ptr << endl;
	cout << ptr << endl;

	char *p;
	a(p);
	/* neu truy cap p sau dong nay se bi segment fault */
	/* cout << p; => segment fault */
	p = a(p);
	cout << p << endl;


	/* fix */
	b(&p);
	cout << p;
	free(p);

	return 0;
}