#include <iostream>

using namespace std;

int main (void)
{
	void *v_p;
	int *a = NULL;
	float *b = NULL;
	int x = 23;
	float y = 4.4;

	v_p = &x;	// valid
	v_p = &y;	// valid
	// a = &y;	: invalid
	// b = &x;	: invalid

	v_p = &x;	// valid
	cout << *(int*)v_p;

	return 0;	
}