#include <stdio.h>

#define PIRNT(a, b) printf("%d %d", a, b)
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define str(x) #x

#define CONCANATE(x, y) x##y

#define EAT(what) puts("I'm eating" #what "today\n")

void function_name (void);


int main (void)
{
	int a = 4, b = 5;
	PIRNT(a, b);

	printf(str(\nHello\n));

	EAT(fruit);

	int xy = 10;
	printf("%d", CONCANATE(x, y));  		// <=> printf("%d\n", xy);

	printf(__FILE__);
	putchar('\n');
	printf(__TIME__);
	putchar('\n');
	printf(__DATE__);
	putchar('\n');
	function_name();
	putchar('\n');

	return 0;
}


void function_name (void)
{
	printf("%s was called\n", __func__);
}