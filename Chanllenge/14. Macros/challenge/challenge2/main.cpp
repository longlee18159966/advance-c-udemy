#include <stdio.h>

#define SUM(a, b) ((a) + (b))

int main (void)
{
	int a, b;
	printf("Enter a and b\n");
	scanf("%d %d", &a, &b);
	printf("a + b = %d + %d = %d\n", a, b, SUM(a, b));

	return 0;
}