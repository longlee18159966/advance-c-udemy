#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <iostream>

using namespace std;

#define IS_LOWERS(c) 			 ((c) >= 'a' ? (((c) <= 'z') ? true : false) : false)
#define IS_UPPERS(c) 			 ((c) >= 'A' ? (((c) <= 'Z') ? true : false) : false)
#define IS_ALPHABETS(c) 		 (IS_LOWERS(c) ? true : (IS_UPPERS(c) ? true : false))
#define IS_DIGITS(c) 			 ((c) >= '0' ? (((c) <= '9') ? true : false) : false)
#define IS_ALPHANUMERICS(c) 	 (IS_ALPHABETS(c) ? true : (IS_DIGITS(c) ? true : false))
#define IS_VOWELS(c) 			 (IS_LOWERS(c) ? ((c) == 'a' ? true : ((c) == 'e' ? true : ((c) == 'i' ? true : ((c) == 'o' ? true : ((c) == 'u' ? true : ((c) == 'y' ? true : false)))))) : false)
#define IS_CONSONANTS(c) 		 (IS_VOWELS(c) ? false : true)
#define IS_SPECIAL_CHARACTERS(c) (IS_ALPHANUMERICS(c) ? false : (IS_WHITESPACE(c) ? false : true))
#define IS_WHITESPACE(c) 		 ((c) == ' ' ? true : ((c) == '\t' ? true : ((c) == '\n' ? true : false)))

int main (void)
{
	char ch;
	while ((ch = getchar()) != '#')
	{
		fflush(stdin);
		if (IS_ALPHABETS(ch))
			cout << "true" << endl;
		else
			cout << "false" << endl;	
	}	

	return 0;
}