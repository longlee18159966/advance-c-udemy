#include <stdio.h>

#define SQUARE(x) ((x)*(x))

#define CUBE(x) (SQUARE(x)*(x))

int main (void)
{
	printf("%d %d %d %d\n", SQUARE(3), CUBE(3), SQUARE(2+4), CUBE(2+4));

	return 0;
}
