/* 
 * Write a program that creates a variadic function that 
 * will allow a programmer to add any amount of numbers(integers) 
 * that they would like to be added together inorder to know how
 * many numbers are being passed to this variadic function, 
 * you can use the first arguments as the number of arguments
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

int32_t addingNumbers (int16_t number_argument, ...);


int main (void)
{
	addingNumbers(2, 10, 20);
	addingNumbers(3, 10, 20, 30);
	addingNumbers(4, 10, 20, 30, 40);

	return 0;
}

int32_t addingNumbers (int16_t number_argument, ...)
{
	va_list parg;	
	int32_t sum = 0;
	int8_t count = 0;
	va_start(parg, number_argument);
	sum += va_arg(parg, int32_t);
	while (++count < number_argument)
	{
		sum += va_arg(parg, int32_t);
		printf("%d\n", sum);
	}

	va_end(parg);

	return sum;
}