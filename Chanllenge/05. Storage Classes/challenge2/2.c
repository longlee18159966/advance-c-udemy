#include <stdio.h>

int sum (int num);


int main (void)
{
    printf("%d\n", sum(20));
    printf("%d\n", sum(30));
    printf("%d\n", sum(40));
}

int sum (int num)
{
    static int total = 0;
    total   += num;
    return total;
}