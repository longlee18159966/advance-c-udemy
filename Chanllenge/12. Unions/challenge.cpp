/* 
 * Write a program which will define union and then use this union to assign 
 * and access its members
 * you must define a union named student that consists of the following three elements:
 * char letterGrade
 * int roundedGrade
 * float exactGrade
 *
 * your program should declare two union variables inside the main method 
 * 
 * your program should assign random values to variable 1
 * You then need to display each value for each member of this union
 * You should notice that something is wrong 
 * 
 * your program should then assign a value (using variable 2) to its member letter grade and then print it out
 * your program should then assign a value (using variable 2) to its member rounded grade and then print it out
 * your program should then assign a value (using variable 2) to its member exact grade and then print it out
 * you should notice the difference in output with variable 1 and variable 2, why is there a differnce ?
 */

/* 
 * Example output:
 * Union record1 values:
 * Letter Grade: 
 * Rounded Grade: 1118633984
 * Exact Grade: 86.500000
 *
 * Union record2 values:
 * Letter Grade: A
 * Rounded Grade: 100
 * Exact Grade: 99.500000
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

typedef union
{
	char letterGrade;
	int roundedGrade;
	float exactGrade;
} student;



int main (void)
{
    int i = 2;
    ++i;
	student variable1, variable2;
	cout << "Union record1 values:" << endl;
	cout << "Letter Grade: " << variable1.letterGrade << endl;
	cout << "Rounded Grade: " << variable1.roundedGrade << endl;
	cout << "Exact Grade: " << variable1.exactGrade << endl;

	cout << "Union record2 values:" << endl;
	variable2.letterGrade	= 'A';
	cout << "Letter Grade: " << variable2.letterGrade << endl;
	variable2.roundedGrade	= 100;
	cout << "Rounded Grade: " << variable2.roundedGrade << endl;
	variable2.exactGrade = 99.5;
	cout << "Exact Grade: " << variable2.exactGrade << endl;

	return 0;
}