
/* 
 * Write a C program to input any number from user
 * the program should chek whether nth bit of the given number is set or not
 * the program should set nth bit of given number as 1
 */
#include <iostream>
#include <stdint.h>

using namespace std;


int main (void)
{
    uint32_t number;
    int bitcheck;
    cout << "Enter the number you want: ";
    cin >> number;
    cout << "Enter nth bit to check and set (0-31): ";
    cin >> bitcheck;
    uint32_t number1 = number;
    if (!(number & (1 << bitcheck)))
    {
        cout << "The " << bitcheck << " bit is set to 0" << endl;
        number1 |= 1 << bitcheck;
        cout << "Bit set successfully" << endl;
    }
    else
    {
        cout << "The " << bitcheck << " bit is set to 1" << endl;
    }

    cout << "Number before setting " << bitcheck << " bit: " << number << "(in decimal)" << endl;
    cout << "Number after setting " << bitcheck << " bit: " << number1 << "(in decimal)" << endl;

    return 0;
}