
/* 
 * Write a program that contains the following bit fields in a struture (onscreen box) 
 * the box can opaque or transparent
 * a fill color is selected from the following palette of corlors: black, red, green, yellow, blue, magenta, cyan or white
 * a border can be shown or hidden
 * a border color is selected from the same pallete used for the fill color
 * a border can use one of the three line styles: solid, dotted or dashed
 */


#include <iostream>

using namespace std;

typedef struct 
{
    unsigned int states         : 1;
    unsigned int fill_colors    : 3;
    unsigned int border_states  : 1;
    unsigned int border_colors  : 3;
    unsigned int line_styles    : 2;
} box_t;

enum States       {opaque = 0, transparent};
enum colors       {black = 0, red, green, yellow, blue, magenta, cyan, white};
enum BorderStates {shown = 0, hidden};
enum LineStyles   {solid = 0, dotted, dashed};

const char *state[] = {"opaque", "transparent"};

const char *borderstate[] = {"shown", "hidden"};

const char *linestyle[] = {"solid", "dotted", "dashed"};

const char *color[] = {"black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"};

int main (void)
{
    box_t onscreen_box = {
                            .states        = opaque,
                            .fill_colors   = green,
                            .border_states = hidden,
                            .border_colors = yellow,
                            .line_styles   = solid
                         };
    cout << "Origrinal box settings: " << endl;
    cout << "Box is " << state[onscreen_box.states] << '.' << endl;
    cout << "The fill color is " << color[onscreen_box.fill_colors] << '.' << endl;
    cout << "Border " << borderstate[onscreen_box.border_states] << '.' << endl;
    cout << "The border color is " << color[onscreen_box.border_colors] << '.' << endl;
    cout << "The border style is " << linestyle[onscreen_box.line_styles] << '.' << endl;

    onscreen_box = {transparent, white, shown, magenta, solid};
    cout << "\n *********************************************** \n";
    cout << "Modified box settings: " << endl;
    cout << "Box is " << state[onscreen_box.states] << '.' << endl;
    cout << "The fill color is " << color[onscreen_box.fill_colors] << '.' << endl;
    cout << "Border " << borderstate[onscreen_box.border_states] << '.' << endl;
    cout << "The border color is " << color[onscreen_box.border_colors] << '.' << endl;
    cout << "The border style is " << linestyle[onscreen_box.line_styles] << '.' << endl;

    return 0;
}